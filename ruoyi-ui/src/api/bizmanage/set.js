import request from '@/utils/request'

// 查询用户列表
export function listReceiver(apikey) {
  return request({
    url: '/mixotcapi/api/biz/accountphonelist',
    method: 'post',
    data: { 'apikey': apikey }
  })
}

// 查询用户列表
export function listReceiverOld(managerPhone) {
  return request({
    url: '/mixapp/bizmanage/receiver/list',
    method: 'get',
    params: { 'managerPhone': managerPhone }
  })
}
// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/system/user/' + praseStrEmpty(userId),
    method: 'get'
  })
}

// 新增用户
export function addReceiver(data) {
  return request({
    url: '/mixapp/bizmanage/receiver/add',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateReceiver(data) {
  return request({
    url: '/mixapp/bizmanage/receiver/update',
    method: 'post',
    data: data
  })
}

// 删除用户
export function delReceiver(data) {
  return request({
    url: '/mixapp/bizmanage/receiver/delete',
    method: 'post',
    data: data
  })
}

// 设置划转方式开关
export function setTransferSwitch(managerPhone, switchOn) {
  return request({
    url: '/mixapp/bizmanage/switch/set',
    method: 'get',
    params: {'managerPhone':managerPhone, 'switchOn':switchOn}
  })
}

// 获取划转方式开关
export function getTransferSwitch(managerPhone) {
  return request({
    url: '/mixapp/bizmanage/switch/get',
    method: 'get',
    params: {'managerPhone':managerPhone}
  })
}