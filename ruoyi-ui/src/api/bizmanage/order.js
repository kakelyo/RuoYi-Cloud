import request from '@/utils/request'
import { formatDate } from "@/utils/index";

// 查询订单列表
export function listOrder(apikey,startDay,endDay,page,size,phone, tradePair) {
  let jsonData = {
    "apikey":apikey,
    "startday":formatDate(new Date(new Date(startDay+' 00:00:00')-8*3600000)), // 数据库存的是utc时间，转成北京时  
    "endday":endDay + ' 16:00:00',
    "page":page,
    "countperpage":size,
    "sourcecurrency":"cny",
    "targetcurrency":"usdt"
  }
  if (phone !== undefined && phone.trim().length>0){
    jsonData.phone = phone.trim()
  }

  if(apikey === "976c4e8fdde94643bf0927440976f6c8"){
    // sero api 特殊设置    
    jsonData.sourcecurrency = 'usdt';
    jsonData.targetcurrency = 'susd';
    if(tradePair === 'SUDT兑USDT'){
      jsonData.sourcecurrency = 'susd';
      jsonData.targetcurrency = 'usdt'
    }
    return request({
      url: '/ortotcapi/api/biz/orders',
      method: 'post',
      data: jsonData
    })
  }else{

    return request({
      url: '/mixotcapi/api/biz/orders',
      method: 'post',
      data: jsonData
    })
  }
}

export function searchOrderByTradeId(apikey, tradeid) {
  if(apikey === "976c4e8fdde94643bf0927440976f6c8"){
    return request({
      url: '/ortotcapi/api/biz/orderdetail',
      method: 'post',
      data: {
        "apikey":apikey,
        "tradeid":tradeid
      }
    })
  }else{
  return request({
      url: '/mixotcapi/api/biz/orderdetail',
      method: 'post',
      data: {
        "apikey":apikey,
        "tradeid":tradeid
      }
    })
  }
}

export function getProfits(apikey) {


  return request({
    url: '/ortotcapi/api/biz/profits',
    method: 'post',
    data: {
      "apikey":apikey,
      "sourcecurrency":'usdt', 
      "targetcurrency":'susd', 
      "sign":"M9m3eB1M/Lx81GQfjphaXADuM/bqSP1tcXECg2X+wRlfOXNXpCAxAoXoEwyniYtaDrQ35EgnHN97C77qwAz/axkKkdPuxC68MHiGdonZZMSlpRUAKAvvlM+FhDdeht00veEdiRM0Q9NnJvGsy/slfRAfwoU5Lm6RqhjlZFlZnmM=",
      "signatureversion":"1.0",
      "signaturemethod":"SHA256WithRSA"
  }
  })
}

