import request from '@/utils/request'

// 查询用户列表
export function getBalance(receiverPhones) {
  return request({
    url: '/mixapp/bizmanage/balance',
    method: 'get',
    params: { 'receiverPhones': receiverPhones }
  })
}
// 查询用户列表
export function getReceiver(apikey) {
  return request({
    url: '/mixotcapi/api/biz/accountphonelist',
    method: 'post',
    data: { 'apikey': apikey }
  })
}
